//
//  TJAppDelegate.h
//  GameTest
//
//  Created by Marwan Al Masri on 5/23/14.
//  Copyright (c) 2014 Marwan Al Masri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TJAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
