//
//  main.m
//  GameTest
//
//  Created by Marwan Al Masri on 5/23/14.
//  Copyright (c) 2014 Marwan Al Masri. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TJAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TJAppDelegate class]));
    }
}
